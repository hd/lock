# lock

A simple Golang Lock. As simple as it gets.

```go
package main

import "gitlab.com/hd/lock"

func main() {
	l := lock.New()

	go func() {
		l.Unlock()
	}()

	l.Wait()
}

```
