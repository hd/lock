package lock

type Lock chan bool

func New() Lock {
	return make(Lock)
}

func (l Lock) Wait() {
	<-l
}

func (l Lock) Unlock() {
	l <- true
}
