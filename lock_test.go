package lock

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNew(t *testing.T) {
	assert.NotNil(t, New())
}

func TestLock_WaitUnlockCross(t *testing.T) {
	l1 := make(Lock)
	l2 := make(Lock)

	go func() {
		l1.Wait()
		l2.Unlock()
	}()

	l1.Unlock()
	l2.Wait()
}

func TestLock_WaitUnlock(t *testing.T) {
	l := New()

	go func() {
		l.Unlock()
	}()

	l.Wait()
}
